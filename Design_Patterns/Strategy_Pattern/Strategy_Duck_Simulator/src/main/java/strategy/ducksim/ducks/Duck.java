package strategy.ducksim.ducks;

import strategy.ducksim.fly.FlyBehavior;
import strategy.ducksim.quack.QuackBehavior;

/**
 * Definition of Duck Template.
 * Main declaration of Duck Simulator.
 * <br>
 * Implementations are in the SubTypes of Duck and in the
 * concrete Implementations of the specified behaviours.
 * <br>
 * The Duck-Simulator is based on the code examples of Head First Design Patterns Strategy chapter.
 * <br>
 * <a href=https://resources.oreilly.com/examples/9780596007126/raw/master/examples/HeadFirstDesignPatterns_code102507.zip></a>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.02.18 v1
 */
public abstract class Duck {
	/**
	 * Uses concrete strategy implementation of FlyBehavior.
	 * It is dynamically set upon execution and is allowed to be changed
	 * at runtime.
	 */
	FlyBehavior flyBehavior;
	QuackBehavior quackBehavior;
 
	public Duck() {
	}

	/**
	 * Dynamical setting of FlyBehavior using an Interface better
	 * than implementing for every SubType of Duck
	 * @param fb Concrete Implementation of {@link FlyBehavior}
	 */
	public void setFlyBehavior (FlyBehavior fb) {
		flyBehavior = fb;
	}

	/**
	 * Dynamical setting of QuackBehavior using an Interface better
	 * than implementing for every SubType of Duck
	 * @param qb Concrete Implementation of {@link QuackBehavior}
	 */
	public void setQuackBehavior(QuackBehavior qb) {
		quackBehavior = qb;
	}

	/**
     * Outputs a message, that represents the duck.
     * <br>
	 * Therefore every Subtype of Duck needs to implement this method.
	 */
	public abstract void display();

	/**
	 * Executes dynamically set flyBehavior. Not necessary to be changed if implementing new flyBehavior.
	 */
	public void performFly() {
		flyBehavior.fly();
	}

	/**
	 * Executes dynamically set quackBehavior. Not necessary to be changed if implementing new quackBehavior.
	 */
	public void performQuack() {
		quackBehavior.quack();
	}

	/**
	 * No need for another implementation, because (for now) all ducks can swim.
	 */
	public void swim() {
		System.out.println("All ducks float, even decoys!");
	}
}
