package strategy.ducksim.simulator;

import strategy.ducksim.ducks.DecoyDuck;
import strategy.ducksim.ducks.MallardDuck;
import strategy.ducksim.ducks.ModelDuck;
import strategy.ducksim.ducks.RubberDuck;
import strategy.ducksim.fly.FlyRocketPowered;

public class MiniDuckSimulator {

	/**
	 * Should not be used, see testcases.
	 */
	/**
	public static void main(String[] args) {
 
		MallardDuck mallard = new MallardDuck();
		RubberDuck rubberDuckie = new RubberDuck();
		DecoyDuck decoy = new DecoyDuck();
 
		ModelDuck model = new ModelDuck();

		mallard.performQuack();
		rubberDuckie.performQuack();
		decoy.performQuack();
   
		model.performFly();	
		model.setFlyBehavior(new FlyRocketPowered());
		model.performFly();
	}
	 */
}
