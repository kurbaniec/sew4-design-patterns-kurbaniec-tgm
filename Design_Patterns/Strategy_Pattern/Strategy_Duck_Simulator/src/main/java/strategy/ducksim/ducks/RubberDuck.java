package strategy.ducksim.ducks;

import strategy.ducksim.fly.FlyNoWay;
import strategy.ducksim.quack.Squeak;

/**
 * Concrete implementation of the abstract model {@link Duck}.
 * <br>
 * The Class represents a rubber duck, a toy rubber duck.
 * <br>
 * This means the duck cannot fly, but can squeak and swim.
 */
public class RubberDuck extends Duck {

    /**
     * Creates a new ModelDuck-object, a duck implementation that cannot fly but quack.
     * <br>
     * Uses the implementation {@link FlyNoWay} for {@link strategy.ducksim.fly.FlyBehavior}.
     * <br>
     * Uses the implementation {@link Squeak} for {@link strategy.ducksim.quack.QuackBehavior}.
     */
	public RubberDuck() {
		flyBehavior = new FlyNoWay();
		quackBehavior = new Squeak();
	}

    /**
     * Outputs a message, that represents the duck.
     */
	public void display() {
		System.out.println("I'm a rubber duckie");
	}
}
