package strategy.ducksim.quack;

/**
 * Concrete implementation of {@link QuackBehavior}.
 * <br>
 * This class is a viable strategy that is used for ducks with fake quacks.
 */
public class FakeQuack implements QuackBehavior {
	public void quack() {
		System.out.println("Qwak");
	}
}
