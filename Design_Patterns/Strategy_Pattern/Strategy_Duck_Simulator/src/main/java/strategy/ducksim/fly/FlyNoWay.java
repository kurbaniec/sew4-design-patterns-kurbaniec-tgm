package strategy.ducksim.fly;

import strategy.ducksim.fly.FlyBehavior;

/**
 * Concrete implementation of {@link FlyBehavior}.
 * <br>
 * This class is a viable strategy that is used for non-flying ducks.
 */
public class FlyNoWay implements FlyBehavior {
	public void fly() {
		System.out.println("I can't fly");
	}
}
