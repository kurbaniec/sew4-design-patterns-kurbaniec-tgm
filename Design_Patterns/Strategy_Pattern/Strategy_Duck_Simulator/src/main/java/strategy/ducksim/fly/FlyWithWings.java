package strategy.ducksim.fly;

import strategy.ducksim.fly.FlyBehavior;

/**
 * Concrete implementation of {@link FlyBehavior}.
 * <br>
 * This class is a viable strategy that is used for normal-flying ducks.
 */
public class FlyWithWings implements FlyBehavior {
	public void fly() {
		System.out.println("I'm flying!!");
	}
}
