package strategy.ducksim.fly;

/**
 * Interface that defines the method {@link FlyBehavior#fly()} to implement.
 * <br>
 * This class therefore describes a strategy-template, that a concrete class uses to execute {@link FlyBehavior#fly()},
 * which needs to be a concrete implementation of {@link FlyBehavior}.
 */
public interface FlyBehavior {

    /**
     * Outputs a message about the fly-abilities of a duck.
     */
	public void fly();
}
