package strategy.ducksim.ducks;

import strategy.ducksim.fly.FlyNoWay;
import strategy.ducksim.quack.MuteQuack;

/**
 * Concrete implementation of the abstract model {@link Duck}.
 * <br>
 * The Class represents a decoy duck, a man-made object resembling a duck.
 * <br>
 * This means the duck cannot fly nor quack, but it can swim.
 */
public class DecoyDuck extends Duck {

    /**
     * Creates a new DecoyDuck-object, a duck implementation that cannot fly nor quack.
     * <br>
     * Uses the implementation {@link FlyNoWay} for {@link strategy.ducksim.fly.FlyBehavior}.
     * <br>
     * Uses the implementation {@link MuteQuack} for {@link strategy.ducksim.quack.QuackBehavior}.
     */
    public DecoyDuck() {
		setFlyBehavior(new FlyNoWay());
		setQuackBehavior(new MuteQuack());
	}

    /**
     * Outputs a message, that represents the duck.
     */
	public void display() {
		System.out.println("I'm a duck Decoy");
	}
}
