package strategy.ducksim.quack;

/**
 * Concrete implementation of {@link QuackBehavior}.
 * <br>
 * This class is a viable strategy that is used for ducks with normal quacks.
 */
public class Quack implements QuackBehavior {
	public void quack() {
		System.out.println("Quack");
	}
}
