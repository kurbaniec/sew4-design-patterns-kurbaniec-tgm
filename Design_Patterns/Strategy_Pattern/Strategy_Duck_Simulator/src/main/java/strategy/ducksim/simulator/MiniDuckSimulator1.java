package strategy.ducksim.simulator;

import strategy.ducksim.ducks.Duck;
import strategy.ducksim.ducks.MallardDuck;
import strategy.ducksim.ducks.ModelDuck;
import strategy.ducksim.fly.FlyRocketPowered;

public class MiniDuckSimulator1 {

	/**
	 * Should not be used, see testcases.
	 */
	/**
	public static void main(String[] args) {
 
		Duck mallard = new MallardDuck();
		mallard.performQuack();
		mallard.performFly();
   
		Duck model = new ModelDuck();
		model.performFly();
		model.setFlyBehavior(new FlyRocketPowered());
		model.performFly();

	}
	 */
}
