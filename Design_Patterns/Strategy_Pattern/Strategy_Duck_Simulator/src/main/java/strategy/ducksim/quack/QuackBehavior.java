package strategy.ducksim.quack;

/**
 * Interface that defines the method {@link QuackBehavior#quack()} to implement.
 * <br>
 * This class therefore describes a strategy-template, that a concrete class uses to execute {@link QuackBehavior#quack()},
 * which needs to be a concrete implementation of {@link QuackBehavior}.
 */
public interface QuackBehavior {

    /**
     * Outputs a message about the quack-abilities of a duck.
     */
	public void quack();
}
