# "*Strategy - Duck Simulator*"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Tests ausführen:

```
gradlew test jacocoTestReport // Mit Wrapper + Coverage
gradle test jacocoTestReport // Coverage
gradle test // Nur Tests
```

## Quellen

