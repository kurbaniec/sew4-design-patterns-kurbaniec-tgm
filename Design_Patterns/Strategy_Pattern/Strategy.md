# Strategy Pattern

## Allgemein

Das Strategy-Pattern definiert eine Familie von Algorithmen, kapselt sie einzeln und macht sie austauschbar. 

Eine Klasse, nennen wir sie `Context`, besitzt als Attribut ein Interface `Strategy`. Jeder zu erstellende Algorithmus implementiert dieses Interface und beinhaltet eine konkrete Methode z.B. `execute` des Interfaces. Jetzt kann in er Klasse Kontext für `Strategy` eine konkrete Implementierung per Setter-Methode gewählt bzw. neu-gewählt werden. Zum Benutzen eines Algorithmus wird in `Context` eine Methode z.B. `executeStrategy` erstellt, die die Methode `execute` des `Strategy`-Attributs aufruft. 

![Strategy-UML](resources/strategy.png)

*Möglicher Aufbau für das Strategy-Pattern*

### Design Prinzipien

* **Identify the aspects of your application that vary and seperate them from what stays the same.**   
  Was fix bleiben soll, soll getrennt werden von Code, der veränderlich sein kann.
* **Program to an interface, not an implementation**    
  Verhalten soll mit Interfaces definiert werden, die von konkreten Klassen implementiert werden. Zur Ausführung kann das Interface per Polymorphie verwendet werden.
* **Favor composition over inheritance**    
  *Hat-Ein* kann *Ist-Ein* überlegen sein.

## Code Examples

* [**Strategy - Calculator**](strategy_calculator/)    
  Ein einfacher Rechner der Rechenoperationen als konkrete Implementierungen eines Strategy-Interfaces besitzt.
* [**Strategy - Duck Simulator**](Strategy_Duck_Simulator)    
  Beispiel für das Strategy-Pattern aus dem Buch **Head First - Design Patterns**.

## Quellen

* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)
* [Das Strategy Design Pattern - Philipp Hauer](https://www.philipphauer.de/study/se/design-pattern/strategy.php)
* [Strategy - Vince Huston](http://www.vincehuston.org/dp/strategy.html)
