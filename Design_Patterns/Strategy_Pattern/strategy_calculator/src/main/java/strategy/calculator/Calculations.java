package strategy.calculator;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * Definition of Calculations.
 * <br>
 * This Interface defines a strategy template. Classes which implement
 * this interface create then the strategy algorithm for a use case.
 * <br>
 * @author Kacper Urbaniec
 * @version 11.02.2019
 */
public interface Calculations <T extends  Number> {
    /**
     * Takes an List of Values and a modifier-value, that will modify
     * all the list-values individually based on the used strategy.
     * @param values List of values to be modified
     * @param modifier The value of the modifier
     * @return The modified list
     */
    public List<T> processCalculation(List<T> values, T modifier);

    /**
     * Converts a number (subtype of class Number) to an BigDecimal-Object
     * with same value
     * @param number Given Parameter
     * @return Value of given Parameter in BigDecimal representation
     */
    public static BigDecimal converter(Number number) {
        BigDecimal returnValue;
        if(number instanceof BigDecimal) {
            returnValue = (BigDecimal) number;
        }
        else {
            if(number instanceof BigInteger) {
                returnValue = new BigDecimal((BigInteger)number);
            }
            else {
                returnValue = BigDecimal.valueOf(number.doubleValue());
            }
        }
        return returnValue;
    }
}
