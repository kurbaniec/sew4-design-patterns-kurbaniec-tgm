package strategy.calculator;

import java.util.ArrayList;
import java.util.List;

/**
 * Calculator program that uses the strategy pattern.
 * <br>
 * Every calculator has a list of values that are modified by the
 * modifier-value. How they are modified depends on the strategy
 * ({@link Addition}, {@link Division}, {@link Multiplication} {@link Subtraction}).
 * @author Kacper Urbaniec
 * @version 11.02.2019
 */
public class Calculator<T extends Number> {
    private T modifier;
    private List<T> values;
    private Calculations<T> calculations;

    public Calculator() {
        this.values = new ArrayList<>();
    }

    public List<T> processCalculations() {
        return calculations.processCalculation(values, modifier);
    }

    public void setModifier(T modifier) {
        this.modifier = modifier;
    }

    public void addValue(T value) {
        this.values.add(value);
    }

    public void removeValue(T value) {
        this.values.remove(value);
    }

    public void setCalculations(Calculations<T> c) {
        this.calculations = c;
    }

    @Override
    public String toString() {
        return this.values.toString();
    }


}
