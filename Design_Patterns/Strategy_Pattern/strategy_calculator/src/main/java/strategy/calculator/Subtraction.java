package strategy.calculator;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Concrete implementation of {@link Calculations}
 * <br>
 * Defines a strategy for subtraction-calculations.
 * @author Kacper Urbaniec
 * @version 11.02.2019
 */
public class Subtraction implements Calculations<Number> {
    private List<Number> retValue;

    /**
     * Takes an List of Values and a modifier-value, that will substract
     * individually all list-values with the modifier
     * @param values List of values to be modified
     * @param modifier The value of the modifier
     * @return The modified list
     */
    @Override
    public List<Number> processCalculation(List<Number> values, Number modifier) {
        retValue = new ArrayList<>();
        BigDecimal mod = Calculations.converter(modifier);
        for(Number element : values)
            if((element instanceof BigDecimal || element instanceof BigInteger) ||
                    (modifier instanceof BigDecimal || modifier instanceof BigInteger)) {
                BigDecimal el = Calculations.converter(element);
                retValue.add(el.subtract(mod));
            }
            else {
                retValue.add(element.doubleValue() - modifier.doubleValue());
            }
        return retValue;
    }



}
