# Observer Pattern

## Allgemein

Definiert eine "eins-zu-vielen" (1:n bzw. 1:\*) - Beziehung. Das eine Objekt wird `Observerable` bzw. `Subject` genannt. Die anderen Objekte, die das Subject "beobachten", werden `Observer` genannt. Ändert das `Subject` seinen Zustand, so werden alle `Observer` benachrichtigt. Im Grunde kann man sich dies wie ein Zeitungsabo vorstellen.

Hervorzuheben beim Observer-Pattern ist die lose Koppelung zwischen Interfaces und konkreten Klassen.

![Observer-Pattern](resources/observer.png)

*Möglicher Aufbau für das Observer-Pattern - Hinweis: `synchronizeState()` wird öfters als `update` angeführt*

Es gibt zwei Ansätze für das Benachrichtigen:

* Pull-Prinzip:    
Beim Aktualisieren der `Observer` bei `update()`, holen sich diese die Daten des `Subjects` per Getter-Methoden in der Methode.
* Push-Prinzip:   
Beim Aktualisieren der `Observer` bei `update(Param p1, ...)`, holen sich diese die Daten des `Subjects` durch die Parameter der Methode.

Vor/Nachteile der Ansätze:   

* Beim Pull-Prinzip werden Getter-Methoden benötigt, damit beim Aufrufen von `update`, die neuen Daten eingeholt werden können. Kommen neue Informationen hinzu, muss für jede konkrete Implementierung von `Observer` die `update`-Methode dafür adaptiert werden. In einigen Fällen wird aber diese neue Information nicht benötigt, somit muss `update` nicht angepasst werden. Somit kann dies auch ein Vorteil sein.
* Beim Push-Prinzip werden keine Getter-Methoden vom `Subject` benötigt, alle Informationen werden mit der `update`-Methode "mitgeliefert".   
  Wir aber ein neuer Parameter hinzugefügt, muss die `update`-Methode dafür verändert werden. Für die korrekte Bearbeitung müssten aber auch alle konkreten `Observer` Klassen die `update`-Methode adaptieren, wenn sie diese Information weiter verarbeiten möchten. 

![Class-Push](resources/weather_push_uml.svg)
*Observer-Pattern mit Push-Prinzip*

![Class-Pull](resources/weather_pull_uml.svg)
*Observer-Pattern mit Pull-Prinzip*

### Design Prinzipien

* **Strive for loosely coupled designs between objects that interact**  
  Subjekt muss für neue Beobachter nicht verändert werden. Außerdem sind Subjekt und Beobachter unabhängig von einander verwendbar.

## Code Examples

* [**Observer Weather - mit Push Prinzip**](observer_weather_NonGUI)    
  Beispiel für das Observer Pattern aus dem Buch **Head First - Design Patterns**.
* [**Observer Weather - mit Pull Prinzip und GUI**](observer_weather)    
  Beispiel für das Observer Pattern aus dem Buch **Head First - Design Patterns**. Jetzt mit GUI !

## Quellen

* [Observer-Pattern - Vince Huston](http://www.vincehuston.org/dp/observer.html)
* [SimpleChat als JavaFX-Vorlage](https://bitbucket.org/kurbaniec/sew4-simple-chat-kurbaniec-tgm/src/master/)
