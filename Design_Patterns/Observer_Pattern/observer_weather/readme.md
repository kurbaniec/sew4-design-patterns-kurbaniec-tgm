# "*Observer - WeatherData*"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Programm per `gradle run` lauffähig machen:

`build.gradle`:
```java
// Define the main class for the application
mainClassName = 'observer.weather.WeatherStation'
```

Alternative Ausführung

```
gradle weather
gradle weahterHeat
```

UML-Klassendiagramm:
In Astah -> `Tools` -> `Import Java`   
Dann auf importierte Daten `weather` Rechtsklick -> `Auto Create Detailed Class Diagram`

![Class-Push](resources/weather_push_uml.svg)
*Observer-Pattern mit Push-Prinzip*

![Class-Pull](resources/weather_pull_uml.svg)
*Observer-Pattern mit Pull-Prinzip*

## Observer-Pattern:

Definiert eine "eins-zu-vielen" (1:n bzw. 1:\*) - Beziehung. Das eine Objekt wird `Observerable` bzw. `Subject` genannt. Die anderen Objekte, die das Subject "beobachten", werden `Observer` genannt. Ändert das `Subject` seinen Zustand, so werden alle `Observer` benachrichtigt. Im Grunde kann man sich dies wie ein Zeitungsabo vorstellen.

Hervorzuheben beim Observer-Pattern ist die lose Koppelung zwischen Interfaces und konkreten Klassen.

Es gibt zwei Ansätze für das Benachrichtigen:
* Pull-Prinzip:    
Beim Aktualisieren der `Observer` bei `update()`, holen sich diese die Daten des `Subjects` per Getter-Methoden in der Methode.
* Push-Prinzip:   
Beim Aktualisieren der `Observer` bei `update(Param p1, ...)`, holen sich diese die Daten des `Subjects` durch die Parameter der Methode.

Vor/Nachteile der Ansätze:   

* Beim Pull-Prinzip werden Getter-Methoden benötigt, damit beim Aufrufen von `update`, die neuen Daten eingeholt werden können. Kommen neue Informationen hinzu, muss für jede konkrete Implementierung von `Observer` die `update`-Methode dafür adaptiert werden. In einigen Fällen wird aber diese neue Information nicht benötigt, somit muss `update` nicht angepasst werden. Somit kann dies auch ein Vorteil sein.

* Beim Push-Prinzip werden keine Getter-Methoden vom `Subject` benötigt, alle Informationen werden mit der `update`-Methode "mitgeliefert".   
  Wir aber ein neuer Parameter hinzugefügt, muss die `update`-Methode dafür verändert werden. Für die korrekte Bearbeitung müssten aber auch alle konkreten `Observer` Klassen die `update`-Methode adaptieren, wenn sie diese Information weiter verarbeiten möchten. 

Zur Anzeige der Daten wird ein JavaFX-Display verwendet. Dieses habe ich auf dem von SimpleChat aufgebaut und auf die Bedürfnisse adaptiert. Das Ergebnis sieht so aus:   

![weatherDisplay](resources/weatherDisplay.png)
*WeatherDisplay auf Basis von JavaFX. In der linken Spalte ist der Log, in der rechten Spalte sind die benutzen Displays zu sehen*

Im `Controller` verwende ich eine Methode zum Hinzufügen der Logs, die aus einzelnen Labels in einem `VBox`-Layout bestehen. Die Display werden einer `ListView` hinzugefügt:

```java
public void addLabel(String text) {
        //Platform.runLater(() -> textArea.appendText("\n" + text));
        Platform.runLater(() -> {
            Label label = new Label(text);
            label.setWrapText(true);
            vbox.getChildren().add(label);
        });
    }

    public void addDisplay(String user) {
        Platform.runLater(() -> {
            ObservableList list = listView.getItems();
            if(!list.contains(user)) {
                list.add(user);
            }
        });
    }
```

Die Daten werden von `Observern` in der `update`-Methode übergeben. Dabei wird der Name (durch `toString()`) und die Ausgabe (durch `displayFX()`, wie `display()` nur mit String-Ausgabe) an den Controller übergeben:

```java
public void update() {
    lastPressure = currentPressure;
    currentPressure = weatherData.getPressure();
    display();
    weatherData.getController().addDisplay(toString());
    weatherData.getController().addLabel(displayFX());
}
```

## Quellen

* [Observer-Pattern - Vince Huston](http://www.vincehuston.org/dp/observer.html)
* [SimpleChat als JavaFX-Vorlage](https://bitbucket.org/kurbaniec/sew4-simple-chat-kurbaniec-tgm/src/master/)
