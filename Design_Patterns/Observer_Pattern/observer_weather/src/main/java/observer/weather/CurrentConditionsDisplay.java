package observer.weather;

/**
 * Concrete implementation of {@link Observer}. Also implements {@link DisplayElement#display()}.
 * <br>
 * In the Observer-Pattern, there is always one {@link Subject}
 * which is "observed" by one or many Observer.
 * <br>
 * When the Subject changes its state all registered Observers are
 * informed via {@link Observer#update()}.
 * <br>
 * CurrentConditionDisplay is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
	private float temperature;
	private float humidity;
	private Subject weatherData;

	/**
	 * Creates a new CurrentConditionsDisplay.
	 * @param weatherData Subject that is observer.
	 */
	public CurrentConditionsDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	/**
	 * Method that {@link WeatherData} invkokes, to share the new
	 * data with the observer.
	 */
	public void update() {
		WeatherData temp = (WeatherData)weatherData;
		this.temperature = temp.getTemperature();
		this.humidity = temp.getHumidity();
		display();
		temp.getController().addDisplay(toString());
		temp.getController().addLabel(displayFX());
	}

	/**
	 * Called to visualize the data of a specific Observer.
	 */
	public void display() {
		System.out.println("Current conditions: " + temperature
			+ "F degrees and " + humidity + "% humidity");
	}

	/**
	 * Called to visualize the data of a specific Observer in JavaFX.
	 * @return String to output
	 */
	public String displayFX() {
		return this.toString() + ":\nCurrent conditions: " + temperature +
				"F degrees and " + humidity + "% humidity";
	}

	/**
	 * Returns class-name
	 * @return Class-name as String
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
