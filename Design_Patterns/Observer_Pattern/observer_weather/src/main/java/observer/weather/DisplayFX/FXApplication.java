package observer.weather.DisplayFX;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import observer.weather.WeatherData;

/**
 * JavaFX-GUI, that forms the WeatherData-Display.
 * <br>
 * Based on <a href="https://bitbucket.org/kurbaniec/sew4-simple-chat-kurbaniec-tgm/src/master/">SimpleChat</a>.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.13 v1
 */
public class FXApplication extends Application {

    private Controller controller;
    private static WeatherData weatherData;
    private Parent root;

    public FXApplication() {}

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/display.fxml"));
        root = loader.load();

        this.controller = loader.getController();

        controller.setWeatherData(weatherData);
        weatherData.setController(controller);

        Scene scene = new Scene(root, 700, 540);

        stage.setTitle("WeatherDisplay");
        stage.setMinWidth(350);
        stage.setMinHeight(350);

        stage.setScene(scene);
        stage.show();
        stage.toFront();
    }

    public void setWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    @Override
    public void stop(){
        controller.stop();
    }

    public Parent getRoot() {
        return root;
    }
}
