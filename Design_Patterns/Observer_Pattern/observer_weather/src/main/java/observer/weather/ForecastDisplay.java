package observer.weather;

import java.util.*;

/**
 * Concrete implementation of {@link Observer}. Also implements {@link DisplayElement#display()}.
 * <br>
 * In the Observer-Pattern, there is always one {@link Subject}
 * which is "observed" by one or many Observer.
 * <br>
 * When the Subject changes its state all registered Observers are
 * informed via {@link Observer#update()}.
 * <br>
 * ForecastDisplay is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public class ForecastDisplay implements Observer, DisplayElement {
	private float currentPressure = 29.92f;  
	private float lastPressure;
	private WeatherData weatherData;

	/**
	 * Creates a new ForecastDisplay.
	 * @param weatherData Subject that is observer.
	 */
	public ForecastDisplay(WeatherData weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	/**
	 * Method that {@link WeatherData} invkokes, to share the new
	 * data with the observer.
	 */
	public void update() {
		lastPressure = currentPressure;
		currentPressure = weatherData.getPressure();
		display();
		weatherData.getController().addDisplay(toString());
		weatherData.getController().addLabel(displayFX());
	}

	/**
	 * Called to visualize the data of a specific Observer.
	 */
	public void display() {
		System.out.print("Forecast: ");
		if (currentPressure > lastPressure) {
			System.out.println("Improving weather on the way!");
		} else if (currentPressure == lastPressure) {
			System.out.println("More of the same");
		} else if (currentPressure < lastPressure) {
			System.out.println("Watch out for cooler, rainy weather");
		}
	}

	/**
	 * Called to visualize the data of a specific Observer in JavaFX.
	 * @return String to output
	 */
	public String displayFX() {
		String output = this.toString() + ":\n";
		if (currentPressure > lastPressure) {
			output += "Improving weather on the way!";
		} else if (currentPressure == lastPressure) {
			output += "More of the same";
		} else if (currentPressure < lastPressure) {
			output += "Watch out for cooler, rainy weather";
		}
		return output;
	}

	/**
	 * Returns class-name
	 * @return Class-name as String
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName();	}
}
