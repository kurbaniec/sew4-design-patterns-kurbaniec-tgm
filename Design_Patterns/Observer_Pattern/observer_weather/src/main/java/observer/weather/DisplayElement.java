package observer.weather;

/**
 * Defines a method, that {@link Observer} implement to visualize their data.
 * <br>
 * DisplayElement is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public interface DisplayElement {
	/**
	 * {@link Observer} calls this, to visualize their data.
	 */
	public void display();
}
