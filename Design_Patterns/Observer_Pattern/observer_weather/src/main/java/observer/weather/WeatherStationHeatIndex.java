package observer.weather;

import observer.weather.DisplayFX.FXApplication;

import java.util.*;

/**
 * Main class to run the Weather Station (with Head Index).
 */
public class WeatherStationHeatIndex {

	public static void main(String[] args) {
		WeatherData weatherData = new WeatherData();
		FXApplication fxApplication = new FXApplication();
		fxApplication.setWeatherData(weatherData);


		CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay(weatherData);
		StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
		ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);
		HeatIndexDisplay heatIndexDisplay = new HeatIndexDisplay(weatherData);

		// Start GUI and make some changes
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				fxApplication.main(args);
			}
		});
		t.start();
		try {
			Thread.sleep(1000);
			weatherData.setMeasurements(80, 65, 30.4f);
			weatherData.setMeasurements(82, 70, 29.2f);
			weatherData.setMeasurements(78, 90, 29.2f);
			t.join();
		}
		catch (Exception ex) {
			System.out.println(ex);
		}
	}
}
