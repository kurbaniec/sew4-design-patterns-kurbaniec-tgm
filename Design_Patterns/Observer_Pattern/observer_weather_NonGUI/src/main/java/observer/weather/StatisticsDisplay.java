package observer.weather;

import java.util.*;

/**
 * Concrete implementation of {@link Observer}. Also implements {@link DisplayElement#display()}.
 * <br>
 * In the Observer-Pattern, there is always one {@link Subject}
 * which is "observed" by one or many Observer.
 * <br>
 * When the Subject changes its state all registered Observers are
 * informed via {@link Observer#update(float, float, float)}.
 * <br>
 * StatisticsDisplay is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public class StatisticsDisplay implements Observer, DisplayElement {
	private float maxTemp = 0.0f;
	private float minTemp = 200;
	private float tempSum= 0.0f;
	private int numReadings;
	private WeatherData weatherData;

	/**
	 * Creates a new StatisticsDisplay.
	 * @param weatherData Subject that is observer.
	 */
	public StatisticsDisplay(WeatherData weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	/**
	 * Method that {@link WeatherData} invkokes, to share the new
	 * data with the observer.
	 * @param temp New temperature-value
	 * @param humidity New humidity-value
	 * @param pressure New pressure-value
	 */
	public void update(float temp, float humidity, float pressure) {
		tempSum += temp;
		numReadings++;

		if (temp > maxTemp) {
			maxTemp = temp;
		}
 
		if (temp < minTemp) {
			minTemp = temp;
		}

		display();
	}

	/**
	 * Called to visualize the data of a specific Observer.
	 */
	public void display() {
		System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
			+ "/" + maxTemp + "/" + minTemp);
	}
}
