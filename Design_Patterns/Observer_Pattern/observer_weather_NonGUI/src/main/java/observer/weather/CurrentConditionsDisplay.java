package observer.weather;

/**
 * Concrete implementation of {@link Observer}. Also implements {@link DisplayElement#display()}.
 * <br>
 * In the Observer-Pattern, there is always one {@link Subject}
 * which is "observed" by one or many Observer.
 * <br>
 * When the Subject changes its state all registered Observers are
 * informed via {@link Observer#update(float, float, float)}.
 * <br>
 * CurrentConditionDisplay is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
	private float temperature;
	private float humidity;
	private Subject weatherData;

	/**
	 * Creates a new CurrentConditionsDisplay.
	 * @param weatherData Subject that is observer.
	 */
	public CurrentConditionsDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	/**
	 * Method that {@link WeatherData} invkokes, to share the new
	 * data with the observer.
	 * @param temperature New temperature-value
	 * @param humidity New humidity-value
	 * @param pressure New pressure-value
	 */
	public void update(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		display();
	}

	/**
	 * Called to visualize the data of a specific Observer.
	 */
	public void display() {
		System.out.println("Current conditions: " + temperature 
			+ "F degrees and " + humidity + "% humidity");
	}
}
