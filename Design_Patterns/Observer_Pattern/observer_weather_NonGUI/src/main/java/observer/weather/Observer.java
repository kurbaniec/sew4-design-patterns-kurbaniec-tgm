package observer.weather;

/**
 * Defines a main template for all Subjects/Observers.
 * <br>
 * In the Observer-Pattern, there is always one {@link Subject}
 * which is "observed" by one or many Observer.
 * <br>
 * When the Subject changes its state all registered Observers are
 * informed via {@link Observer#update(float, float, float)}.
 * <br>
 * Observer is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public interface Observer {
	/**
	 * Method the a concrete Subject invokes on every Observer, to
	 * share the new data.
	 * @param temp New temperature-value
	 * @param humidity New humidity-value
	 * @param pressure New pressure-value
	 */
	public void update(float temp, float humidity, float pressure);
}
