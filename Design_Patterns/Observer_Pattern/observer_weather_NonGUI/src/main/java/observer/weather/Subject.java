package observer.weather;

/**
 * Defines a main template for all Subjects/Observers.
 * <br>
 * In the Observer-Pattern, there is always one Subject
 * which is "observed" by one or many {@link Observer}.
 * <br>
 * When the Subject changes its state in a concrete implementation
 * all registered Observers need to be notified.
 * <br>
 * Subject is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public interface Subject {
	/**
	 * Registers a new {@link Observer}.
	 * @param o Observer that wants to be registered.
	 */
	public void registerObserver(Observer o);

	/**
	 * Removes a {@link Observer}.
	 * @param o Observer that wants to be removed.
	 */
	public void removeObserver(Observer o);

	/**
	 * When the Subject changes it´s state, all registered
	 * {@link Observer} need to be notified.
	 */
	public void notifyObservers();
}
