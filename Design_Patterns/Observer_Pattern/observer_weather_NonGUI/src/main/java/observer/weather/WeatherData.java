package observer.weather;

import java.util.*;

/**
 * Concrete implementation of {@link Subject}.
 * <br>
 * In the Observer-Pattern, there is always one Subject
 * which is "observed" by one or many {@link Observer}.
 * <br>
 * When a WeatherData-object changes its state
 * all registered Observers need to be notified.
 * <br>
 * WeatherData is based on the code examples of Head First Design Patterns Observer chapter.
 * <br>
 * @author Kacper Urbaniec {@literal <kurbaniec@tgm.ac.at>}
 * @version 2019.03.11 v1
 */
public class WeatherData implements Subject {
	// Collection that stores all registered observers
	private ArrayList observers;
	private float temperature;
	private float humidity;
	private float pressure;

	/**
	 * Creates a new WeatherData-Object
	 */
	public WeatherData() {
		observers = new ArrayList();
	}

	/**
	 * Registers a new {@link Observer}.
	 * @param o Observer that wants to be registered.
	 */
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	/**
	 * Removes a {@link Observer}.
	 * @param o Observer that wants to be removed.
	 */
	public void removeObserver(Observer o) {
		int i = observers.indexOf(o);
		if (i >= 0) {
			observers.remove(i);
		}
	}

	/**
	 * When the Subject changes it´s state, all registered
	 * {@link Observer} need to be notified.
	 * <br>
	 * The underlying {@link Observer#update(float, float, float)}
	 * method works with a push-principle.
	 */
	public void notifyObservers() {
		for (int i = 0; i < observers.size(); i++) {
			Observer observer = (Observer)observers.get(i);
			observer.update(temperature, humidity, pressure);
		}
	}

	/**
	 * Method the Subject invokes, when some data is changes.
	 */
	public void measurementsChanged() {
		notifyObservers();
	}

	
	public void setMeasurements(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
	
	public float getTemperature() {
		return temperature;
	}
	
	public float getHumidity() {
		return humidity;
	}
	
	public float getPressure() {
		return pressure;
	}
}
