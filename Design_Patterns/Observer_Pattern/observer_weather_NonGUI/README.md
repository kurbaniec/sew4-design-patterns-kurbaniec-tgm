# "*Observer - WeatherData*"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Programm per `gradle run` lauffähig machen:

`build.gradle`:
```java
// Define the main class for the application
mainClassName = 'observer.weather.WeatherStation'
```

Alternative Ausführung

```
gradle weather
gradle weahterHeat
```

UML-Klassendiagramm:
In Astah -> `Tools` -> `Import Java`   
Dann auf importiere Daten `weather` Rechtsklick -> `Auto Create Detailed Class Diagram`

![Class-Push](resources/weather_push_uml.svg)
*Observer-Pattern mit Push-Prinzip*

## Observer-Pattern:

Definiert eine "eins-zu-vielen" (1:n bzw. 1:\*) - Beziehung. Das eine Objekt wird `Observerable` bzw. `Subject` genannt. Die anderen Objekte, die das Subject "beobachten", werden `Observer` genannt. Ändert das `Subject` seinen Zustand, so werden alle `Observer` benachrichtigt. Im Grunde kann man sich dies wie ein Zeitungsabo vorstellen.

Hervorzuheben beim Observer-Pattern ist die lose Koppelung zwischen Interfaces und konkreten Klassen. 

Es gibt zwei Ansätze für das Benachrichtigen:
* Pull-Prinzip:    
Beim Aktualisieren der `Observer` bei `update()`, holen sich diese die Daten des `Subjects` per Getter-Methoden in der Methode.
* Push-Prinzip:
Beim Aktualisieren der `Observer` bei `update(Param p1, ...)`, holen sich diese die Daten des `Subjects` durch die Parameter der Methode.

## Quellen
* [Observer-Pattern](http://www.vincehuston.org/dp/observer.html)
