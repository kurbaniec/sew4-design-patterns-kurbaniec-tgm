# Decorator Pattern

## Allgemein

**Wieso wird das Decorator Pattern überhaupt verwendet?**   
Grundidee ist die, dass man einen Objekt bei Laufzeit zusätzliches Verhalten hinzufügen möchte. Vererbung kann daher keine Lösung sein, weil sie statisch arbeitet.

**Aufbau**   
Ganz oben in der Hierarchie steht ein Interface bzw. eine abstrakte Klasse (`Component`). Dieses definiert Methoden, die weiter "**ausgeschmückt**" werden können. Diese erbt bzw. implementiert eine Klasse, die die Grundfunktionalität bereitstellen möchte, also das Grundverhalten (`Core`). Daneben gibt es ein Interface was wiederum von `Core` erbt oder implementiert, was aber keine gewöhnliche Klasse ist, sondern eine abstrakte. Dies ist unser Decorator, er besitzt außerdem als Attribut ein `Component`-Objekt selbst. Jetzt kann man mehrere Klassen erstellen, die den `Decorator` implementieren und neue zusätzliche konkrete Verhalten bereitstellen. Vor bzw. nach jeder konkreten Implementierungen muss im Methoden-Körper dieselbe Methode über das `Component`-Object aufgerufen werden,  nur dadurch können die verschiedenen *Decorator*-Implementierungen und der `Core` zusammen arbeiten. 

![Decorator-UML](resources/decPattern.png)

![Decorator-UML](resources/decPattern2.png)

*Möglicher Aufbau für das Strategy-Pattern*

### Design Prinzipien

* **Classes should be open for extension, but closed for modification.**   
  Core Implementierung und dazugehörige *Decorator*-Interface bleiben gleich, da sie den Grundstein bilden und daher nicht verändert werden sollten.   
  Zusätzliches, neues Verhalten kann aber per konkreten *Decorator*-Implementierungen erweitert werden!

## Code Examples

* [**Decorator - TextReader**](decorator_textreader/)    
  Programm, das Daten mittels Passwort und Verschlüsselung gegen unerwünschten Zugriff schützt, basierend auf dem Decorator-Pattern.
* [**Decorator - Own IO**](decorator_IO/)    
  Beispiel für das Decorator-Pattern aus dem Buch **Head First - Design Patterns**.

## Quellen

* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)
* [Decorator - Vince Huston](http://www.vincehuston.org/dp/decorator.html)
