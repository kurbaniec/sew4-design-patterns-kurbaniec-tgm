package io;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Kacper Urbaniec
 * @version 2019-04-27
 */
public class InputTest {

    public static void main(String[] args) {
        int c;

        try {
            InputStream in =
                new LowerCaseInputStream(
                    new BufferedInputStream(
                        new FileInputStream(ClassLoader.getSystemResource("test.txt").getFile())));

            while ((c = in.read()) >= 0) {
                System.out.print((char) c);
            }

            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
