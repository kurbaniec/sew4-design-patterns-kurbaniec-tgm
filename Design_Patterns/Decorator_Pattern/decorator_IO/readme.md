# "*Decorator - IO*"

## Allgemein

Java I/O Dekorierer, der den Textinhalt eines Files in Kleinbuchstaben umwandelt.

Ausführen:

```
gradle run
```

## Aufbau

![IO Decorator](resources/decIO1.png)

*IO-Decorator Aufbau*

![IO Decorator](resources/decIO2.png)

*Decorator Pattern Allgemein*

## Quellen

* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)
* [Head-First Design-Patterns Repo](https://github.com/bethrobson/Head-First-Design-Patterns)
