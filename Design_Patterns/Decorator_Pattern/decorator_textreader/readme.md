# "*Decorator - TextReader*"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

### Programm ausführen

```bash
gradle run --console=plain
```

Der Parameter `plain` wird gebraucht, um die Anzeige nach Vorgabe zu erhalten. Ansonsten ist sie komplett verrutscht und durch Gradle Status-Meldungen schwer lesbar.

![](img/program_test.PNG)

*Vollständiger Programmablauf*

### Decorator Pattern

**Wieso wird das Decorator Pattern überhaupt verwendet?**   
Grundidee ist die, dass man einen Objekt bei Laufzeit zusätzliches Verhalten hinzufügen möchte. Vererbung kann daher keine Lösung sein, weil sie statisch arbeitet.

**Aufbau**   
Ganz oben in der Hierarchie steht ein Interface bzw. eine abstrakte Klasse (*Component*). Dieses definiert Methoden, die weiter "**ausgeschmückt**" werden können. Diese erbt bzw. implementiert eine Klasse, die die Grundfunktionalität bereitstellen möchte, also das Grundverhalten (*Core*). Daneben gibt es ein Interface was wiederum von *Core* erbt oder implementiert, was aber keine gewöhnliche Klasse ist, sondern eine abstrakte. Dies ist unser Decorator, er besitzt außerdem als Attribut ein *Component*-Objekt selbst. Jetzt kann man mehrere Klassen erstellen, die den *Decorator* implementieren und neue zusätzliche konkrete Verhalten bereitstellen. Vor bzw. nach jeder konkreten Implementierungen muss im Methoden-Körper dieselbe Methode über das *Component*-Object aufgerufen werden,  nur dadurch können die verschiedenen *Decorator*-Implementierungen und der *Core* zusammen arbeiten. 

### TextReader als Interface umsetzen

Wenn man die abstrakte Klasse `TextReader` als Interface umsetzt, müssen alle Attribute und Methoden `public` gestellt werden (passiert default mäßig). Man kann keine strengeren Access-Modifier einsetzen, da sonst andere Klassen diese nicht mehr implementieren können. 

Als Vorteil gewinnt man aber eine "lockere" Trennung. Außerdem wurde seit Java 8 der große Vorteil von abstrakten Klassen, vollständige Methoden implementieren zu können, auch in Interfaces durch sogenannte *default* Methoden implementiert.

### Testen

In der Klasse `TextReaderTest` finden sich Testfälle, die die Programmfälle testen. Derzeit ist das Problem, dass jeder Test einzeln ausgeführt werden muss. Wenn man alle Tests auf einmal ausführt, funktionieren nach dem ersten alle weiteren nicht mehr, sie *failen*. Das Problem liegt an der Simulation von `System.setIn`, um User-Inputs zu simulieren. `setIn` ist final static, das heißt, dass die erste Eingebe vom simulierten Input funktioniert, alle nachfolgenden werden nicht mehr gesetzt. 

## Quellen

* [Decorator Pattern - Vince Huston](http://www.vincehuston.org/dp/decorator.html)
* [Base64 - In Java implementieren](https://www.baeldung.com/java-base64-encode-and-decode)
* [Base64 - UTF8 Unterstützung](https://stackoverflow.com/questions/4714519/java-base64-utf8-string-decoding)
* [User-Input simulieren](https://stackoverflow.com/questions/1647907/junit-how-to-simulate-system-in-testing)
* [SystemIn](https://stackoverflow.com/questions/7141930/system-setin-reassigns-final-system-in)

