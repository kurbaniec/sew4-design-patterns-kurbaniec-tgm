package decorator.textreader;

import java.io.IOException;

/**
 * Concrete implementation of {@link Decorator}.
 * <br>
 * Is used to secure a text via password.
 * <br>
 * In the Decorator-Pattern this is used to add new
 * functionality to a object. In order to work, it needs
 * to have a instance of the other concrete Decorator or Core
 * (in this case attribute inner), so they can be called to
 * add their functionality.
 * <br>
 * @author Kacper Urbaniec
 * @version 18.03.2019
 */
public class Authentication extends Decorator {
    private String passwd;

    public Authentication(TextReader textReader) {
        inner = textReader;
    }

    @Override
    public void write(String[] s) {
        System.out.print( "PASSWORD:\t" );
        try {
            passwd = in.readLine();
        } catch (IOException ex) { ex.printStackTrace(); }
        inner.write(s);
    }

    @Override
    public void read(String[] s) {
        try {
            boolean flag = true;
            do {
                System.out.print( "PASSWORD:\t" );
                String input = in.readLine();
                if (passwd.equals(input)) {
                    inner.read(s);
                    flag = false;
                }
                else if ("exit".equals(input)) {
                    flag = false;
                    System.out.println("Aborting...");
                }
                else{
                    System.out.println("False PASSWORD - Try again!\nAbort with \"exit\"");
                }
            } while(flag);
        } catch (IOException ex) { ex.printStackTrace(); }
    }
}
