package decorator.textreader;

/**
 * Decorator of {@link TextReader}.
 * <br>
 * In the Decorator-Pattern, a decorator is used to add
 * new functionality through it´s concrete implementations.
 * <br>
 * A decorator always contains a instance of it´s component,
 * in this case {@link TextReader}.
 * <br>
 * @author Kacper Urbaniec
 * @version 18.03.2019
 */
public abstract class Decorator implements TextReader {
    public TextReader inner;

    public abstract void write(String[] s);

    public abstract void read(String[] s);
}
