package decorator.textreader;

import java.util.Base64;

/**
 * Concrete implementation of {@link Decorator}.
 * <br>
 * Is used to encrypt or decrypt a text.
 * <br>
 * In the Decorator-Pattern this is used to add new
 * functionality to a object. In order to work, it needs
 * to have a instance of the other concrete Decorator or Core
 * (in this case attribute inner), so they can be called to
 * add their functionality.
 * <br>
 * @author Kacper Urbaniec
 * @version 18.03.2019
 */
public class Scrambling extends Decorator{

    public Scrambling(TextReader textReader) {
        inner = textReader;
    }

    @Override
    public void write(String[] s) {
        inner.write(s);
        System.out.println( "encrypt:\t\t" );
        s[0] = Base64.getEncoder().encodeToString(s[0].getBytes());
    }

    @Override
    public void read(String[] s) {
        System.out.println( "decrypt:\t\t" );
        byte[] decodedBytes = Base64.getDecoder().decode(s[0]);
        try {
            s[0] = new String(decodedBytes, "UTF-8");
        } catch (Exception ex) { ex.printStackTrace(); }
        inner.read(s);
    }
}
