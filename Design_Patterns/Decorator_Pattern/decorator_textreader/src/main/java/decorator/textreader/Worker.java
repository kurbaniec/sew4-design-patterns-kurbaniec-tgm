package decorator.textreader;

import java.io.IOException;

/**
 * Core-Implementation of {@link TextReader}.
 * <br>
 * Used to read a given input text.
 * <br>
 * In the Decorator-Pattern, this defines the most basic
 * functionality, that can be "decorated" with additional
 * functionality via concrete implementations of {@link Decorator}.
 * <br
 * @author Kacper Urbaniec
 * @version 18.03.2019
 */

public class Worker implements TextReader {
  public void write( String[] s ) {
    System.out.print( "Input:\t\t" );
    try {
      s[0] = in.readLine();
    } catch (IOException ex) { ex.printStackTrace(); }
  }
  public void read( String[] s ) {
    System.out.print( "Output:\t\t" + s[0] );
  }
}
