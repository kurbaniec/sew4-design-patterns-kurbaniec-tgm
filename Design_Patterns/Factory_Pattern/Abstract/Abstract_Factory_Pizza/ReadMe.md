# "*Abstract Factory Pizza*"

## Allgemein

Beispiel für eine Abstract Factory Pizza-Fabrik.

Ausführen:

```
gradle run
```

## Quellen

* [Head-First Design-Patterns Repo](https://github.com/bethrobson/Head-First-Design-Patterns)
* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)

