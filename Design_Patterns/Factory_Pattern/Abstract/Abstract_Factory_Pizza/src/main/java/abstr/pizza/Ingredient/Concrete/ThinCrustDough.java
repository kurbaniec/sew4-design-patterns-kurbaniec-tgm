package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Dough;

public class ThinCrustDough implements Dough {
	public String toString() {
		return "Thin Crust Dough";
	}
}
