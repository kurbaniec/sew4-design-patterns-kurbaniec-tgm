package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Dough;

public class ThickCrustDough implements Dough {
	public String toString() {
		return "ThickCrust style extra thick crust dough";
	}
}
