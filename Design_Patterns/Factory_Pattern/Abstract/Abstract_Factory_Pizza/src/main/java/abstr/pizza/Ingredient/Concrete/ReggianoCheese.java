package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Cheese;

public class ReggianoCheese implements Cheese {

	public String toString() {
		return "Reggiano Cheese";
	}
}
