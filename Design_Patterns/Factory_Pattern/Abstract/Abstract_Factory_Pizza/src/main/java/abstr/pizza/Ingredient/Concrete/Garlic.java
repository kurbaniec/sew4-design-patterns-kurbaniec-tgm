package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Veggies;

public class Garlic implements Veggies {

	public String toString() {
		return "Garlic";
	}
}
