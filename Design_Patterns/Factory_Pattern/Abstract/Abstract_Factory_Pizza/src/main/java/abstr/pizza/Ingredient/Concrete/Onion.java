package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Veggies;

public class Onion implements Veggies {

	public String toString() {
		return "Onion";
	}
}
