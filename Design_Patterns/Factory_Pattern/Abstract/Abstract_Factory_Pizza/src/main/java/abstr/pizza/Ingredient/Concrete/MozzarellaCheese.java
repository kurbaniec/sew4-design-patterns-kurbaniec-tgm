package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Cheese;

public class MozzarellaCheese implements Cheese {

	public String toString() {
		return "Shredded Mozzarella";
	}
}
