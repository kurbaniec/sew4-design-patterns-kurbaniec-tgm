package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Sauce;

public class PlumTomatoSauce implements Sauce {
	public String toString() {
		return "Tomato sauce with plum tomatoes";
	}
}
