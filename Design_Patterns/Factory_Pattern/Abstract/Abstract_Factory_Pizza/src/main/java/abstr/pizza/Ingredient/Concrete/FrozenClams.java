package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Clams;

public class FrozenClams implements Clams {

	public String toString() {
		return "Frozen Clams from Chesapeake Bay";
	}
}
