package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Pepperoni;

public class SlicedPepperoni implements Pepperoni {

	public String toString() {
		return "Sliced Pepperoni";
	}
}
