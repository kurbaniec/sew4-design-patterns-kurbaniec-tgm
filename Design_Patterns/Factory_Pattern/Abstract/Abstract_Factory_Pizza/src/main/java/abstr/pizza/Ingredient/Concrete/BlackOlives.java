package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Veggies;

public class BlackOlives implements Veggies {

	public String toString() {
		return "Black Olives";
	}
}
