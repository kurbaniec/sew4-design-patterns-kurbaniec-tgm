package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Clams;

public class FreshClams implements Clams {

	public String toString() {
		return "Fresh Clams from Long Island Sound";
	}
}
