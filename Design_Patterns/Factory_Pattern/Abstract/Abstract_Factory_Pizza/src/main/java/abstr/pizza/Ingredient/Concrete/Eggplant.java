package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Veggies;

public class Eggplant implements Veggies {

	public String toString() {
		return "Eggplant";
	}
}
