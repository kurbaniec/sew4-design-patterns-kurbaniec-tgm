package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Cheese;

public class ParmesanCheese implements Cheese {

	public String toString() {
		return "Shredded Parmesan";
	}
}
