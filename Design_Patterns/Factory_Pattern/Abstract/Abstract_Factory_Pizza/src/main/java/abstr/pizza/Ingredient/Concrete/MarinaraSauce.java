package abstr.pizza.Ingredient.Concrete;

import abstr.pizza.Ingredient.Sauce;

public class MarinaraSauce implements Sauce {
	public String toString() {
		return "Marinara Sauce";
	}
}
