# Abstract Factory Pattern

## Allgemein

Bei der Abstrakten Fabrik erstellt man Produkte aus aus mehreren Unterprodukten zusammen. Diese werden durch verschiedene konkrete Fabriken bereitgestellt. Wenn wir auf unser Pizza-Thema zurückgehen, so werden die Pizzen nun durch verschiedene Zutaten erstellt, die durch regionale Fabriken bereitgestellt werden. Im Code selbst wird nur auf eine abstrakte Fabrik verwiesen, erst zur Laufzeit wird die richtige, regionale Fabrik benutzt. 

![pizza](resources/pizza.png)

*Erstellung eines konkreten Produktes*

Somit kann man sich den Ablauf einer Abstrakten Fabrik so vorstellen:

![pizza](resources/ablauf.png)

### Prinzip

![Prinzip](resources/prinzip.png)

*Möglicher Aufbau für das Abstract Factory Pattern* 

**Akteure**

* `AbstrakteFabrik`    
  Definiert eine Schnittstelle zur Erzeugung abstrakter Produkte einer Produktfamilie.
* `KonkreteFabrik`   
  Erzeugt konkrete Produkte einer Produktfamilie durch Implementierung der Schnittstelle.
* `AbstraktesProdukt`   
  Definiert eine Schnittstelle für eine Produktart.
* `KonkretesProdukt`   
  Definiert ein konkretes Produkt einer Produktart durch Implementierung der Schnittstelle, wird durch die korrespondierende konkrete Fabrik erzeugt.
* `Klient`   
  Verwendet die Schnittstellen der abstrakten Fabrik und der abstrakten Produkte.

### Design Prinzipien

![Design](resources/design.png)

## Code Examples

* [**Abstract Factory Pizza**](Abstract_Factory_Pizza/)    
  Beispiel für eine Abstract Factory aus dem Buch **Head First - Design Patterns**.

### Extra - komplette Abstract Pizza Factory aus Head First

Persönlich finde ich, dass man das Pattern einfach visuell betrachten sollte, um dies einfacher zu verstehen.

![Abstract Factory 1](resources/af1.png)

![Abstract Factory 1](resources/af2.png)

![Abstract Factory 1](resources/af3.png)

## Quellen

* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)
* [Head-First Design-Patterns Repo](https://github.com/bethrobson/Head-First-Design-Patterns)
* [Abstrakte Fabrik](<https://de.wikipedia.org/wiki/Abstrakte_Fabrik>)
