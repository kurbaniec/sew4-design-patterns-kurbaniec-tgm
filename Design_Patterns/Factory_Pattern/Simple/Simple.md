# Simple Factory

## Allgemein

Die Simple Factory dient zum Programmieren auf Implementierungen.

Die Simple Factory ist per se kein Pattern, sie hilft aber beim offen-geschlossen Prinzip. Denn Änderungen müssen nur an einer Stelle durchgeführt werden.

Bei der Simple Factory besitzt man eine Klasse z.B. `SimplePizzaFactory`, die beispielhaft eine Methode `createPizza(String type)` enthält, die eine Pizza zum übergebenen Typ zurückgibt. Somit muss man nicht `new` benutzen. 

![Simple](resources/simple.png)

*Möglicher Aufbau für eine Simple Factory*

### Design Prinzipien

* **Classes should be open for extension but closed for modification.**
* **Attach additional responsibilites to an object dynamically. Decoratos provide a flexible alternative to subclassing for extending functionality.**

## Code Examples

* [**Simple Factory Pizza**](Simple_Factory_Pizza/)    
  Beispiel für eine Simple Factory aus dem Buch **Head First - Design Patterns**.

## Quellen

* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)
* [Head-First Design-Patterns Repo](https://github.com/bethrobson/Head-First-Design-Patterns)
