# "*Simple Pizza Factory*"

## Allgemein

Beispiel für eine einfache Pizza-Fabrik.

*Achtung:* Simple-Factory != Pattern

Ausführen:

```
gradle run
```

## Quellen

* [Head-First Design-Patterns Repo](https://github.com/bethrobson/Head-First-Design-Patterns)
* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)

