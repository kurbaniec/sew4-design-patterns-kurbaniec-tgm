package method.pizza.NY;

import method.pizza.NY.Type.NYStyleCheesePizza;
import method.pizza.NY.Type.NYStyleClamPizza;
import method.pizza.NY.Type.NYStylePepperoniPizza;
import method.pizza.NY.Type.NYStyleVeggiePizza;
import method.pizza.Pizza;
import method.pizza.PizzaStore;

public class NYPizzaStore extends PizzaStore {

	@Override
	public Pizza createPizza(String item) {
		if (item.equals("cheese")) {
			return new NYStyleCheesePizza();
		} else if (item.equals("veggie")) {
			return new NYStyleVeggiePizza();
		} else if (item.equals("clam")) {
			return new NYStyleClamPizza();
		} else if (item.equals("pepperoni")) {
			return new NYStylePepperoniPizza();
		} else return null;
	}
}
