package method.pizza.Chicago;

import method.pizza.Chicago.Type.ChicagoStyleCheesePizza;
import method.pizza.Chicago.Type.ChicagoStyleClamPizza;
import method.pizza.Chicago.Type.ChicagoStylePepperoniPizza;
import method.pizza.Chicago.Type.ChicagoStyleVeggiePizza;
import method.pizza.Pizza;
import method.pizza.PizzaStore;

public class ChicagoPizzaStore extends PizzaStore {

	public Pizza createPizza(String item) {
        	if (item.equals("cheese")) {
            		return new ChicagoStyleCheesePizza();
        	} else if (item.equals("veggie")) {
        	    	return new ChicagoStyleVeggiePizza();
        	} else if (item.equals("clam")) {
        	    	return new ChicagoStyleClamPizza();
        	} else if (item.equals("pepperoni")) {
            		return new ChicagoStylePepperoniPizza();
        	} else return null;
	}
}
