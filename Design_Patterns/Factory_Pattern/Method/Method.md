# Factory Method Pattern

## Allgemein

Die Factory Method ist eine Erweiterung der Simple Factory.

Der große Unterschied ist jetzt, dass wir einen abstrakten `Creator` besitzen z.B. `PizzaStore`. Dieser gibt die Fabrikmethode vor z.B. `abstract Pizza createPizza(String item)`. Dadurch können jetzt mehrere verschiedene konkrete Erzeuger benutzt werden wie `ChicagoPizzaStore` oder `NYPizzaStore`.

![Prinzip](resources/method2.png)

*Prinzip*

![Method](resources/method.png)

*Möglicher Aufbau für das Factory Method Pattern* 

### Design Prinzipien

* **Classes should be open for extension but closed for modification.**
* **Attach additional responsibilites to an object dynamically. Decoratos provide a flexible alternative to subclassing for extending functionality.**
* **Depend upon abstractions. Do not depend upon concrete classes.**   
  ![Erklärung](resources/method3.png)

## Code Examples

* [**Factory Method Pizza**](Factory_Method_Pizza/)    
  Beispiel für eine Simple Factory aus dem Buch **Head First - Design Patterns**.

## Quellen

* [Head First Design Patterns - O'Reilly](http://shop.oreilly.com/product/9780596007126.do)
* [Head-First Design-Patterns Repo](https://github.com/bethrobson/Head-First-Design-Patterns)
* [Fabrikmethode](https://de.wikipedia.org/wiki/Fabrikmethode)
