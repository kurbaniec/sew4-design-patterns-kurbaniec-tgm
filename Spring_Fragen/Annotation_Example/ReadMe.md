# "*Custom Annotation Example - JSON-Serializer*"

## Allgemein

Einfaches Beispiel für Custom Annotations von [Baeldung](https://www.baeldung.com/).

Ausführen:

```
gradle run
```

## Quellen

* [Creating a Custom Annotation in Java](https://www.baeldung.com/java-custom-annotation)
* [Baeldung-Repo](https://github.com/eugenp/tutorials/tree/master/core-java-8/src/main/java/com/baeldung/customannotations)

