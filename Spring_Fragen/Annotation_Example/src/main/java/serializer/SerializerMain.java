package serializer;

/**
 * Small test for serializer
 * @author Kacper Urbaniec
 * @version 2019-04-28
 */
public class SerializerMain {

    public static void main(String[] args) {
        Person person = new Person("Kacper", "Urbaniec", "18");
        System.out.println("Object: " + person.toString());
        ObjectToJsonConverter serializer = new ObjectToJsonConverter();
        String jsonString = serializer.convertToJson(person);
        System.out.println("Json: " + jsonString);
    }
}
