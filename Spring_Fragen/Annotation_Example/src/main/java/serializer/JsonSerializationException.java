package serializer;

/*
 * Source: https://www.baeldung.com/java-custom-annotation
 */
public class JsonSerializationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public JsonSerializationException(String message) {
        super(message);
    }
}
