package serializer;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/*
 * Used to call init Methods, like initNames in Person, that
 * need to be called before conversion
 * Source: https://www.baeldung.com/java-custom-annotation
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Init {

}
