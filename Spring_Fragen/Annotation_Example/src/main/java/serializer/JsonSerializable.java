package serializer;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/*
 * Mark class with fields that should be converted to JSON
 * Source: https://www.baeldung.com/java-custom-annotation
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface JsonSerializable {

}
