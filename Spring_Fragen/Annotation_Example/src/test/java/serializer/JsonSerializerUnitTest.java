package serializer;

import static org.junit.Assert.assertEquals;
import org.junit.Test;



public class JsonSerializerUnitTest {

    @Test(expected = JsonSerializationException.class)
    public void givenObjectNotSerializedThenExceptionThrown() {
        Object object = new Object();
        ObjectToJsonConverter serializer = new ObjectToJsonConverter();
        serializer.convertToJson(object);
        /*assertThrows(JsonSerializationException.class, () -> {
            serializer.convertToJson(object);
        });*/
    }

    @Test
    public void givenObjectSerializedThenTrueReturned() throws JsonSerializationException {
        Person person = new Person("soufiane", "cheouati", "34");
        ObjectToJsonConverter serializer = new ObjectToJsonConverter();
        String jsonString = serializer.convertToJson(person);
        assertEquals("{\"personAge\":\"34\",\"firstName\":\"Soufiane\",\"lastName\":\"Cheouati\"}", jsonString);
    }
}
