# Spring

## Aufgabenstellung

Folgende Fragen sind ausschließlich über die Spring-Documentation und -API zu recherchieren und in einem README.md zu dokumentieren:

1. Beschreibe folgende Annotationen
   * @SpringBootApplication
   * @Bean
   * @RestController
   * @RequestMapping
2. Was sind Annotationen?
3. Welche Möglichkeit gibt es, diese zu implementieren?
4. Wie hängen Annotationen mit Interfaces zusammen? Welche Gemeinsamkeiten gibt es?
5. Kann man die @SpringBootApplication auch anders anschreiben? 
6. Welche @Target Optionen gibt es und was bedeuten diese?
7. Was hat @Retention zu bedeuten?

---

1. Wo erwartet sich SpringBoot das application.properties-File?
2. Ist dies auch für das gradle-Plugin so?
3. Wie kann man eigene Configuration.Settings im Spring-Boot setzen?
4. Wo kann man die wichtigsten Default-Configurations von Spring-Boot vorfinden?

## Implementierung

1. Beschreibe folgende Annotationen

   - **@SpringBootApplication**      
     Vereinigt `@Configuration`, `@EnableAutoConfiguration`, und`@ComponentScan`, Annotations die erlauben extra  Konfiguration zu landen, Spring´s automatischen Konfiguration-Mechanismus zu starten und den *Component*-Scan zu aktivieren, dieser wird benötigt, damit Spring annotierte Klassen managen kann (`@Configuration`, `@Service`, `@Controller`, `@Bean` und so weiter).

   - **@Bean**    
     Meist über einer Getter-Methode verwendet. Besagt, dass das zurückgegebene Objekte durch den Spring IoC (Inversion of Control) Container gemanaged werden werden soll. Dadurch kann dann z.B. `getBean()` oder `@Autowired` verwendet werden.

   - **@RestController**    
     Annotation die besagt, dass es sich bei der Klasse um einen Controller (`@Controller`) handelt, wobei alle Methoden mit `@RequestMapping`standardmäßig mit `@ResponeBody` behandelt werden. Das heißt die Anfragen die man implementiert werden automatisch de-serialisiert. Somit wird mit `return "Hallo"` eine Webpage mit dem Text *Hallo* zurückgegeben.

   - **@RequestMapping**     
     Wird über Methoden angegeben in einem Controller, die Anfragen bearbeiten sollen. Dabei wird z.B. per `value` der Pfad angeben.   
     Kleines Beispiel:   

     ```java
     // test with curl -i http://localhost:8080/spring-rest/ex/foos
     @RequestMapping(value = "/ex/foos", method = RequestMethod.GET)
     @ResponseBody
     public String getFoosBySimplePath() {
         return "Get some Foos";
     }
     // test with curl -i -X POST http://localhost:8080/spring-rest/ex/foos
     @RequestMapping(value = "/ex/foos", method = POST)
     @ResponseBody
     public String postFoos() {
         return "Post some Foos";
     }
     ```

2. Kann man die @SpringBootApplication auch anders anschreiben?   

   ```
   @Configuration
   @EnableAutoConfiguration
   @ComponentScan
   ```

3. Was sind Annotationen?     
   Annotationen sind eine Form von Metadaten. Sie bieten Programm-Daten an, die nicht teil des Programmes selber sind. Somit haben Annotation keinen direkten Einfluss auf den Code, denn sie kommentieren (=Annotate).    

   

   Annotationen haben eine Reihe von Einsatzmöglichkeiten wie:

   * Informationen für den Compiler
   * Compile-time and Deployment-time proccessing
   * Runtime processing

   

   In Java existieren einige vorgefertigte Annotationen wie:

   * @Override   
     Dies informiert den Compiler, dass die annotierte Methode eine aus der Superklasse überschreibt. Grundsätzlich ist dies zwingend nicht notwendig, doch überschreibt man was nicht korrekt, wirft der Compiler schon bei der Auswertung einen Fehler. Somit dient diese Annotation zur Fehlervermeidung.
   * @Deprecated     
     Falls das annotierte Element im Programm benutzt wird, gibt der Compiler bei der Ausführung diesen eine Meldung aus, dass man dieses Element nicht benutzen sollte.

   

4. Wie hängen Annotationen mit Interfaces zusammen? Welche Gemeinsamkeiten gibt es?     
   Annotations sind Interfaces, die durch die JVM im Hintergrund implementiert werden.       
   Unterschiede Annotation - Interface: 

   * Das Schlüsselwort `interface` ist gleich, nur es wird ein `@` vorangestellt. 

   * Nur Methoden ohne Argumente und `throws` sind erlaubt.

   * Rückgabewert der Methoden dürfen alle primitiven Datentypen, String, Class, Enumerationen, Annotationen und Arrays der eben genannten sein

   * Methoden können einen „default“-Wert besitzen.

     

5. Welche @Target Optionen gibt es und was bedeuten diese?     
   Wird ebenfalls für Annotationen angewandt. Das `@Target` sagt dem Compiler, welche Elemente eine Annotation er markieren darf. `@Target` erwartet ein Element von `ElementType`.

   1. `ElementType.ANNOTATION_TYPE` can be applied to an annotation type.
   2. `ElementType.CONSTRUCTOR` can be applied to a constructor.
   3. `ElementType.FIELD` can be applied to a field or property.
   4. `ElementType.LOCAL_VARIABLE` can be applied to a local variable.
   5. `ElementType.METHOD` can be applied to a method-level annotation.
   6. `ElementType.PACKAGE` can be applied to a package declaration.
   7. `ElementType.PARAMETER` can be applied to the parameters of a method.
   8. `ElementType.TYPE` can be applied to any element of a class.

   

6. Was hat @Retention zu bedeuten?      
   Regelt die Sichtbarkeit einer Annotation. Dazu wird eine `RetentionPolicy` gesetzt.

   - **Source**: Die Annotation ist nur im Quellcode sichtbar

   - **Class**: Die Annotation ist auch in der Class-Datei, also im kompilierten Code, vorhanden.

   - **Runtime**: Die Annotation wird während der Laufzeit von der JVM behalten, so dass sie zur Laufzeit benutzt werden kann. 

     

7. Welche Möglichkeit gibt es, diese selbst zu implementieren?      
   Zuerst muss eine Annotation erstellt werden. Man wählt wie sie behandelt bzw. wie lang gespeichert werden soll (`Retention`) und wo sie angewendet werden kann (`Target`).

   ```java
   import java.lang.annotation.ElementType;
   import java.lang.annotation.Retention;
   import java.lang.annotation.RetentionPolicy;
   import java.lang.annotation.Target;
    
   @Retention(RetentionPolicy.RUNTIME)
   @Target(ElementType.METHOD)
   public @interface IntegerSetter {
     int min();
     int max();
     int start() default 1;
   }
   ```

   Dann fügt man die Annotation hinzu, in diesem Fall zu den Methoden.

   ```java
   public class Point {
    
     private int x, y;
    
     @IntegerSetter(min = 0, max = 5)
     public void setX(int x) {
       this.x = x;
     }
    
     @IntegerSetter(min = 0, max = 20, start = 5)
     public void setY(int y) {
       this.y = y;
     }
   }
   ```

   Zur Laufzeit kann mittels Reflection durch die Klasse die Annotation ausgelesen werden, um dadurch Verhalten zu implementieren.

   ```java
   public static void main(String[] args) throws Exception {
     Point.list(Point.class);
   }
    
   public static void list(Class<?> clazz) throws Exception {
     for (Method method : clazz.getMethods()) {
       IntegerSetter value = method.getAnnotation(IntegerSetter.class);
       if (value != null) {
         System.out.printf("%s: min=%d, max=%d, start=%d\n",
           method.getName(), value.min(), value.max(), value.start());
       }
     }
   }
   ```

   Output:

   ```
   setX: min=0, max=5, start=1
   setY: min=0, max=20, start=5
   ```
   

------

1. Wo erwartet sich SpringBoot das application.properties-File?     
   Das File `application.properties` muss sich irgendwo im Classpath von Spring befinden, um gefunden zu werden. Falls man ein Build-Tool wie Maven/Gradle verwendet, sollte man das File unter `src/main/resources` anlegen.     

2. Ist dies auch für das gradle-Plugin so?   
   Bei Build-Tools wie Gradle wir immer  `src/main/resources` verwendet.     

3. Wie kann man eigene Configuration.Settings im Spring-Boot setzen?    
   Man kann ganz einfach eigene Konfigurationen wie Variablen anlegen:
   
   ```
   app.name=MyApp
   app.description=${app.name} is a Spring Boot application
   ```
   
   Im Code können diese Properties dann so referenziert werden:
   
   ```
   @Value("${app.name}")
   private String userBucketPath;
   ```
   
   Alternativ kann über einer Klasse `@ConfigurationProperties("property-prefix-name")` angegeben werden, dann werden alle Variablen injected, die diesen Präfix tragen:
   
   ```
   @ConfigurationProperties("acme")
   public class AcmeProperties {
   	// Für acme.enabled
   	private boolean enabled;
   	// Für acme.remote-address
   	private InetAddress remoteAddress;
   	...
   ```
   
   Konfigurationen können auch genutzt werden, um Start-Befehle zu kürzen um z.B. `--port=9000` anstatt `--server.port=9000` zu verwenden:
   
   ```
   server.port=${port:8080}
   ```
   
     
   
4. Wo kann man die wichtigsten Default-Configurations von Spring-Boot vorfinden?      
   Diese kann man in Teil 10 der offiziellen Dokumentation unter [Appendix A. Common application properties](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html) finden.

   Dort finden man default-Konfigurationen wie `server.port=8080 # Server HTTP port`.

## Code Examples

Zum Thema Annotations ist [hier](Annotation_Example/) noch ein längeres Beispiel von Baeldung zu finden.

## Resources

[1] [Definition von @SpringBootApplication; Spring Boot Docs; zuletzt besucht am 5.4.2019; online](<https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/SpringBootApplication.html>)  
[2] [Annotation Java-Doc; zuletzt besucht am 5.4.2019; online](<https://docs.oracle.com/javase/tutorial/java/annotations/>)   
[3] [Understanding Annotations - jaxenter; zuletzt besucht am 5.4.2019; online ](<https://jaxenter.com/understand-annotations-java-148001.html>)      
[4] [Create custom Annotations - mkyong; zuletzt besucht am 5.4.2019; online](<https://www.mkyong.com/java/java-custom-annotations-example/>)     
[5] [Location of application.properties; zuletzt besucht am 28.4.2019; online](https://stackoverflow.com/questions/38775194/where-is-the-application-properties-file-in-a-spring-boot-project)   
[6] [Appendix A. Common application properties; zuletzt besucht am 28.4.2019; online](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)      
[7] [Configuration - Own Short Commands; zuletzt besucht am 28.4.2019; online](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-properties-and-configuration.html#howto-use-short-command-line-arguments)    
[8] [Configuration - Own properties; zuletzt besucht am 28.4.2019; online](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html#boot-features-external-config-placeholders-in-properties)    
[9] [Configuration - Access properties; zuletzt besucht am 28.4.2019; online](https://stackoverflow.com/questions/30528255/how-to-access-a-value-defined-in-the-application-properties-file-in-spring-boot)     
[10] [@SpringBootApplication; zuletzt besucht am 28.4.2019; online](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-using-springbootapplication-annotation.html)    
[11] [@RestController; zuletzt besucht am 28.4.2019; online](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/RestController.html)      
[12] [@RequestMapping; zuletzt besucht am 28.4.2019; online](https://www.baeldung.com/spring-requestmapping)   
[13] [Annotations - Erklärungen und Beispiele; zuletzt besucht am 28.4.2019; online](https://www.java-blog-buch.de/0604-annotation/)