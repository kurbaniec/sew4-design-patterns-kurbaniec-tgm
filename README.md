# "*Software Development*"

## Design Patterns

* [Strategy](Design_Patterns/Strategy_Pattern/Strategy.md)
* [Observer](Design_Patterns/Observer_Pattern/Observer.md)
* [Decorator](Design_Patterns/Decorator_Pattern/Decorator.md)
* Factory
    * [Simple](Design_Patterns/Factory_Pattern/Simple/Simple.md)
    * [Method](Design_Patterns/Factory_Pattern/Method/Method.md)
    * [Abstract](Design_Patterns/Factory_Pattern/Abstract/Abstract.md)
* Iterator

## Allgemeines zu Spring

* [Spring Fragen](Spring_Fragen/Spring_Fragen.md)

---

Please feel free to describe the essential Software development principles and patterns. [Here](TASK.md) you will find guidance.  
*"Do. Or do not. There is no try."* [1]

## Resources
[1] The Empire Strikes Back, Yoda.  

